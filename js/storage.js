/**
 * Created by ttt on 25.04.2016.
 */

/**
 * Объект CRUD для работы с localStorage и реализующий интерфейс promise
 * var obj = new Storage();
 * obj.read(section)
 *        .done(function(data){ ... })
 *        .fail(function(e){ alert(e.toString()); })
 *        .always(function(){ ... });
 *
 * @constructor
 */
function myStorage() {
	var self = this;
	var deferred = $.Deferred();
	self.promise = deferred.promise();

	/**
	 * Read
	 * @param section
	 * @returns {*} Promise
	 */
	self.read = function(section){
		try{
			var item = localStorage.getItem(section);
			if(!item){
				throw new Error('section not found');
			}

			item = JSON.parse(item);

			setTimeout(function(){
				deferred.resolve(item);
			}, 1000);
		}catch(e){
			deferred.reject(e);
		}

		return self.promise;
	}


	/**
	 * Create
	 * @param section
	 * @param data
	 * @returns {*} Promise
	 */
	self.create = function(section, data){
		try{
			var item = JSON.stringify(data);
			localStorage.setItem(section, item);

			setTimeout(function(){
				deferred.resolve();
			}, 1000);
		}catch(e){
			deferred.reject(e);
		}

		return self.promise;
	}

	self.update = self.create;

	/**
	 * Delete
	 * @param section
	 * @returns {*} Promise
	 */
	self.delete = function(section){
		try{
			localStorage.removeItem(section);
			setTimeout(function(){
				deferred.resolve();
			}, 1000);
		}catch(e){
			deferred.reject(e);
		}

		return self.promise;
	}
}
