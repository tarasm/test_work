/**
 * Created by ttt on 26.04.2016.
 */

(function(){
// init jGrowl
	$.jGrowl.defaults.pool = 5;

	$.jGrowl.defaults.closerTemplate = '<div>hide all</div>';
	$.jGrowl.life = 20000;
	$.jGrowl.speed = 'slow';
})();

// Перезагрузка страницы с задержкой
function pageReload(timeout, force){
	setTimeout(function(){ location.reload(force); }, timeout );
}

// Очистка локального хранилища
function clearStorage(){
	localStorage.clear();
	$.jGrowl('Clear storage');
	pageReload(2000);
}


function loadData(forms, callback) {
// Загружаем данные на страницу из хранилища

	var promises = jQuery.map(forms, function ( form, section ) {

		return new myStorage().read(section)
			.done(function (data) {
				$.jGrowl('read section ' + section + ' success.');
				jQuery.each(data, function (index, value) {
					form.setVar(index, value);
				});
			}).fail(function (e) {
				$.jGrowl('read ' + section + ' fail. ' + e.toString());
			});
	});

// Если не получилось загрузить из хранилища
	$.when.apply(null, promises).fail(function () {
		// загружаем начальные данные
		$.getJSON('data.json', function (data) {
			$.jGrowl('load init data');

			var promises = jQuery.map(data, function (value_form, index_form) {

				var storage = new myStorage().create(index_form, value_form).fail(function (e) {
					$.jGrowl(e.toString());
				}).done(function () {
					$.jGrowl('set ' + index_form + ' success!');
				});

				return storage;
			});

			$.when.apply(null, promises).done(function () {
				$.jGrowl('load init success');

				pageReload(3000);
			});
		});
	}).done(function(){ callback(); });
}




// Сохранение данных формы в хранилище
function storeData(data, section, callback){
	new myStorage().update(section, data).done(function(){
		$.jGrowl('update success!');

		callback();
	});
}
