/**
 * Created by ttt on 27.04.2016.
 */
var crudAdd = angular.module('crudApp', []);

/**
 * class формы с данными
 * @param $scope
 * @param vars
 * @constructor
 */
function Form($scope, vars, label){
	var self = this;

	self.active = false;

	self.label = label;

	self.vars = vars;

	self.setVar = function(varName, value){
		if(self.vars[varName]){
			self.vars[varName].value = value;
		}else{
			self.vars[varName] = new FormVar(varName, value);
		}
	};
}

/**
 * class поля ввода формы с данными
 * @param label
 * @param value
 * @param readonly
 * @constructor
 */
function FormVar(label, value, readonly){
	var self = this;
	self.label = label;
	self.value = value;
	self.readonly = readonly

	self.toString = function(){ return self.value; };
}

crudAdd.controller('crudAppCtrl', ['$scope', function($scope) {

	$scope.clearAll = clearStorage;

	$scope.forms = {
		"person": new Form($scope, {
			"person_id": new FormVar('Person id',"", true),
			"first_name": new FormVar('First Name'),
			"last_name": new FormVar('Last Name'),
			"middle_name": new FormVar('Middle Name'),
			"email": new FormVar('Email'),
			"phone_number": new FormVar('Phone Number')
		}, 'Person'),
		"user": new Form($scope, {
			"user_id": new FormVar('User id', '', true),
			"nickname": new FormVar('Nickname'),
			"department_id": new FormVar('Department id'),
			"person_id": new FormVar('Person id'),
			"position_id": new FormVar('Position id'),
			"super_user": new FormVar('Super User')
		}, 'User'),
		"position": new Form($scope, {
			"position_id": new FormVar('Position id', '', true),
			"position_name": new FormVar("Position name"),
			"salary": new FormVar('Salary')
		}, 'Position'),
		"department": new Form($scope, {
			"department_id": new FormVar('Department id', '', true),
			"department_name": new FormVar('Department Name'),
			"company_id": new FormVar('Company id')
		}, 'Department'),
		"company": new Form($scope, {
			"company_id": new FormVar('Company id', '', true),
			"company_name": new FormVar('Company Name'),
			"description": new FormVar('Description'),
			"logo": new FormVar('Logo')
		}, 'Company')
	};

	$scope.setActive = function(formName){

		$.jGrowl('set active ' + formName);

		$scope.formActive = $scope.forms[formName];
		$scope.formActiveName = formName;

		// Клонируем активную форму
		var form_vars = { };
		jQuery.each($scope.formActive.vars, function(index, value){
			form_vars[index] = new FormVar(value.label, value.value, value.readonly);
		});

		$scope.modalForm = new Form($scope, form_vars, formName );

		$.each($scope.forms, function(index, form){
			form.active = (index == formName);
		});
	}

	loadData($scope.forms, function(){
		// рендеринг данных на странице при загрузке
		setTimeout(function() {
			$scope.setActive('person');
			$scope.$apply();
		}, 500);
	});

	$scope.form_submit = function(){
		console.log(this);

		activeName = $scope.formActiveName;

		var save_data = { };
		jQuery.each($scope.modalForm.vars, function(index, value){
			save_data[index] = value.value;
		});

		console.log(save_data);

		storeData(save_data,activeName, function(){
			$scope.forms[activeName] = $scope.modalForm;
			$scope.setActive(activeName);
			$scope.$apply();
		})


		$('#modal-form').modal('hide');
		return false;
	}
}]);


